/**
 * @param {string} str
 * @param {string} subStr
 * @returns {boolean}
 */
var string_search = function (str, subStr) {
    const [len_str, len_substr] = [str.length, subStr.length]
    let isMatch;
    let count = 0;
    // let count2 = 0;
    for (let i = 0; i < len_str; i++) {
        isMatch = true
        for (let j = 0; j < len_substr; j++) {
            if (subStr[j] !== str[i + j]) {
                isMatch = false
                break
            }
            // if (subStr[j] === str[i]) {
            //     console.log(str[i + j])
            //     count2 += 1
            // }

        }
        if (isMatch) {
            count += 1
        }

    }
    // console.log(count2 * 2)
    return count > 0;
}

console.log(string_search(
    "i can return return return it",
    "return"
))
