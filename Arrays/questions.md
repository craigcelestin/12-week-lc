# Practice vs Study Questions - Arrays

## Week One - Arrays, Sliding Windows
- Sliding Window involves finding a window in array that satisfies a condition -> subarray between 2 pointers
    * Steps: (1) expand window -> (2) meet condition -> (3) contract window
        - Iterate on this process until we obtain desired result


## Week Two - Two Pointers
- Two pointers are similar to sliding window, that contract towards each other until a condition is hit
    * If you have an array of sorted values and want to find two #s that sum up to a certain value, move pointers depending on sum of current values
    * also fast, slow moving pointers

### Study
- backspace string compare
- 3sum
- sort colors
- container with most water

### Practice, Apply
- two sum
- squares of sorted array
- subarray product < k
- 3sum closest
- trap rain water

## Week Three - Recursion and Backtracking
- Recursion is critical for all three of these domains
```
def recurse():
    < logic before recursive calls >
    recurse()
    < logic after recursive calls >
```
- for backtracking
    ```
    recurse()
     recurse()
      recurse()
       recurse()
        recurse() < each upper step is undoing last step from recursion >
    ```

### Study
- Letter combos of phone #
- combo sum
- permutations
- combinations
- subsets
### Practice, Apply
- generate parens
- combo sum ii
- permutations
- subsets ii
- palindrome partitioning
- target sum
- combo sum iii
- sudoku solver

## Week Four
- Binary search: using two pointers to keep track of middle and understand if middle element = target, get rid of 1/2 of possibilities successively depending on if target is > or < than middle giving log n time complexity

### Study
- find smallest letter greater than target
- find peak element
- peak index in mtn array
- search in rotated sorted array
### Practice, Apply
- find peak element
- first bad version
- search in rotated sorted array 2
- search a 2d matrix
- count of range sum

## Week Five
- STACK = FILO, used to solve several problems optimally

### Study
- valid parens
- max subarray min product
- min add to make parens valid
- merge intervals
- max freq stack

### Practice, Apply
- daily temps
- asteroid collision
- next greater element 2
- 132 pattern
- largest rect in historgram
- number of visible people in a queue

## Week Six
- choose locally optimal decisions to reach a global maxima
    * don't come back to previous choices - opposite of backtracking, no guarantee if you find most overall optimal answer
    - ex: if need to construct largest number from a provided set of digits, greedy algo is a wise choice
- to be able to understand if a greedy approach should be used or not, start/come up with example (test case) where making locally optimal decision won't provide right answer, then greedy is invalid

### Study
- longest palindrome
- shortest unsorted continuous subarray
- valid triangle #
- increasing triplet subsequence
- best time to buy and sell stock ii
### Practice, Apply
- gas station
- largest number
- remove duplicate letters
- remove k digits
- integer replacement
