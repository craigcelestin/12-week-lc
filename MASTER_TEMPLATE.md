# DATE

## []()

### Problem

### Analysis
- Input:
- Output:

- Algo Process
    - Restate Problem

    - Goal of Function

    - Types

    - Assertions and Assumptions

    - Edge Cases

#### Example \#1
```

```

#### Example \#2
```

```
#### Example \#3 & \#4
```

```

#### Constraints

#### _Backside_
- My solution would involve:


#### Additional Details
- Reflecting on Attempt:

### Preliminary Solution
- __Time Complexity__:

- __Space Complexity__:

#### Solution Code
- [JS Solution]()
- [Python Solution]()

### [Neetcode]()
- [Neetcode Solution]()
#### TC, SC


- #### NC Notes
