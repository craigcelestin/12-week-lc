# Practice vs Study Questions - Trees
- Binary Tree = special type of graph
    1. each node except root which has one parent
    2. each node has 0 to 2 children nodes
- Each BT question solely involves their traversal

## Week 7 - Tree BFS (Breadth FIrst/Level Order Traversal)
- Put root first in queue, then process tree level by level
    * nodes that enter Queue first are processed first -> qs involve how we process tree when we push, pop from queue

### Study
- Min depth of binary tree
- binary tree level order traversal
- BT zigzag level order traversal
- BT level order traversal ii

### Practice, Apply
- avg of bt levels
- populate next right pointers /node
- populate next right pointers in each node ii
- bt right side view
- all nodes distance k in bt

## Week [8 - 10] - BT DFS
- Traversing each branch first in terms of node depth
    * 3 methods: inorder, pre, post traversal
        ```
                1
               / \
              2   3
             / \
            4   5

        - preorder: 1,2,4,5,3
            * node, left, right
        - inorder: 4,2,5,1,3
            * process left child, node, right
        - postorder: 4,5,2,3,1
            left, right, node
        ```
    * demo
        ```
        def tree_dfs(node):
            // while traversing down tree
            if node is None:
                return
            // preorder - print(node.value)
            // before going to right subtree
            tree_dfs(node.left)
            // inorder - print(node.value)
            tree_dfs(node.irght)
            // while going up tree
            // postorder - print(node.value)
        ```

### Study
- same tree
- merge two bts
- construct bt from preorder, inorder traversal
- path sum
- bt diameter
- lowest common binary tree ancestor

### Practice, Apply
- max depth of bt
- path sum 2
- bt max path sum
- sum root to leaf #s
- implement trie prefix tree
- kth smallest element in a bst
- invert bst
- lowest common ancestor of bst
- word search 2
- validate binary search tree
- path sum 2
- subtree of other tree
- max bst
- max width of bst
- serialize and deserialize binary tree
