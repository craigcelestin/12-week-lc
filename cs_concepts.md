# [5 Concepts](https://www.youtube.com/watch?v=qjpJ-kfn07o)

## 1. Concurrency v Parallelism
- concurrency = start another task prior to finishing first one i.e. at subsequent points i.e start task a -> pause task a -> start task b -> paused task b -> and so on... (anything async is concurrent)
    * start task before first one concludes
    - Two Options:
        1. interleave between tasks (concurrency not in parallel)
        2. (run tasks at same time) Parallelism is a subset of concurrency where both tasks are running at same time
- two types of tasks that can execute concurrently whether in parallel or not: threads and processes

## 2. Threads and Processes
- a process: executing program and its resources (runtime environment) -
    * __(1) virtual address space, (2)executable code, (3) open handles to sys objects, (4) security context, (5) unique process identifier, (6) env vars, (7) priority class, (8) min and max working set sizes, (9) one thread of execution__
    * each process started with single thread (primary thread), but can create additional threads from any of threads

    - __RESOURCE EXPLANATION__
        1. virtual address space = range of addresses in ram
            * virtual address space: range of addresses mapped from main memory (ram)
        2. executable code - complied code cpu runs
        3. system objects - files, directories, mutexes, registers
        4. security context - ds that determine what resources a process can/can't access
        5. process identifier - a unique numeric value each process has
        6. env variables - process lvl vars that subprocesses inherit
        7. priority class - attribute/value of each process to aid in determining how early/frequently process is to run on cpu relative to others
        8. work set sizes - amt of ram process uses
        9. primary thread - instruction set/unit of execution run by cpu

- A process can have several threads and each thread has same virtual address space of process
    - each has its own priority
    - each has a single set of instructions

#### Conclusion
- Process
    1. has its own address space
    2. can have several threads
- Thread
    1. shares address space w/ other process threads
    2. only single instruction set

## 3. Race Conditions
- Threads that execute either concurrently or in parallel has race conditions
    * when u have two or more threads that share a resource, if 1 thread writes to thread then behavior can be unpredictable
- To prevent any unpredictably as a result of read and writes, use `mutexes` to lock a shared resource so only 1 thread can access at a time

## 4. Garbage Collection
- how programming languages free memory that is not used by unreferenced objects to prevent memory leaks (memory held by processes, but not used)
    * py = runtime env, counts references and keeps track of what to collect when ref_count drops to 0

## 5. Array v Linked List
- arrays in memory in contiguous blocks allowing for O(1), O(n) for adding/removing elements/searching, LL each node has pointers that represent place in memory for each next node -> value of pointer is in virtual address space with O(n) access and O(1) removal/addition
