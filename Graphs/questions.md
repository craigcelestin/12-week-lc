# Practice vs Study Questions - Graphs
- Graphs represented similar to trees, but instead of left and right children
    * each node has value and list of neighboring nodes

## Week 11 - GRAPH BFS, DFS
- BFS use a Queue, DFs involves recurse on neighboring nodes with a matrix on adjacent cells of current element/cell in matrix traversal
    - req some data structure that keep track of visited nodes

### Matrices
#### Study
- flood fill, number of islands, max island area
#### Practice
- island perimeter, pacific atlantic h2o flow, surrounded regions, count sub islands

### Graphs
#### Study
- evaluate division, get watched videos by friends, clone graph, number of operations to make network connected
#### Practice
- merge accounts, # of provinces, course schedule 2, redundant connection

## Week 12 - Graph shortest path

### Study
- network delay time, path with max probability, word ladder, cut off trees for golf event
### Practice
- path with min effort, cheapest flights in k stops, critical connections in network, shortest path to get all keys
    * Dijkstra's: helping in solving many shortest path problems like __network delay time, path with max probability,__ etc.
