#  [12 week lc guide](https://www.youtube.com/watch?v=UKP-Vca9Q4c)

## Plan
- Going to do 3 wk plan to ramp up in time for interviews ( Arrays, Trees, Graphs )
    - Each section involving 2 sets of Qs being: (1) study solutions list, (2) practice questions list
        * __BEING SOLID__: Having LC mastery = solve common medium level Qs in 30 min
    - For big companies, look at most common questions
        - Bloomberg, MongoDB, AWS, Cushman and Wakefield
